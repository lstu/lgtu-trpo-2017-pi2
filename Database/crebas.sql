/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     27.04.2017 22:15:53                          */
/*==============================================================*/


drop table if exists Excercise_plan;

drop table if exists Exercise_attempt;

drop table if exists Exercise_plan;

drop table if exists Exercise_type;

drop table if exists Measurement;

drop table if exists Training_session;

drop table if exists User;

drop table if exists measurment_type;

/*==============================================================*/
/* Table: Excercise_plan                                        */
/*==============================================================*/
create table Excercise_plan
(
   EP_plan_id           int,
   ET_exercise_id       int
);

/*==============================================================*/
/* Table: Exercise_attempt                                      */
/*==============================================================*/
create table Exercise_attempt
(
   EA_attempt           int not null,
   ET_exercise_id       int not null,
   TS_training_id       int,
   EA_quantity_of_repetitions int,
   EA_duration          time,
   EA_weight            int,
   primary key (EA_attempt)
);

/*==============================================================*/
/* Table: Exercise_plan                                         */
/*==============================================================*/
create table Exercise_plan
(
   EP_plan_id           int not null,
   U_user_id            int not null,
   EP_day_of_week       varchar(15),
   primary key (EP_plan_id)
);

/*==============================================================*/
/* Table: Exercise_type                                         */
/*==============================================================*/
create table Exercise_type
(
   ET_exercise_id       int not null,
   ET_muscle_group      varchar(40),
   ET_name              varchar(50),
   ET_description       varchar(500),
   ET_type              varchar(50),
   primary key (ET_exercise_id)
);

/*==============================================================*/
/* Table: Measurement                                           */
/*==============================================================*/
create table Measurement
(
   M_id                 int not null auto_increment,
   M_value              float,
   MT_id                int,
   TS_training_id       int,
   primary key (M_id)
);

/*==============================================================*/
/* Table: Training_session                                      */
/*==============================================================*/
create table Training_session
(
   TS_training_id       int not null,
   EP_plan_id           int,
   TS_training_start    datetime,
   TS_training_end      datetime,
   primary key (TS_training_id)
);

/*==============================================================*/
/* Table: User                                                  */
/*==============================================================*/
create table User
(
   U_user_id            int not null,
   U_username           varchar(50),
   U_password           varchar(32),
   U_first_name         varchar(50),
   U_last_name          varchar(50),
   U_image              varchar(200),
   primary key (U_user_id)
);

/*==============================================================*/
/* Table: measurment_type                                       */
/*==============================================================*/
create table measurment_type
(
   MT_id                int not null auto_increment,
   MT_name              varchar(50),
   primary key (MT_id)
);

alter table Excercise_plan add constraint FK_Reference_11 foreign key (ET_exercise_id)
      references Exercise_type (ET_exercise_id);

alter table Excercise_plan add constraint FK_Reference_6 foreign key (EP_plan_id)
      references Exercise_plan (EP_plan_id);

alter table Exercise_attempt add constraint FK_Reference_8 foreign key (TS_training_id)
      references Training_session (TS_training_id);

alter table Exercise_attempt add constraint FK_contains foreign key (ET_exercise_id)
      references Exercise_type (ET_exercise_id);

alter table Exercise_plan add constraint FK_creates foreign key (U_user_id)
      references User (U_user_id);

alter table Measurement add constraint FK_Reference_12 foreign key (TS_training_id)
      references Training_session (TS_training_id);

alter table Measurement add constraint FK_Reference_9 foreign key (MT_id)
      references measurment_type (MT_id);

alter table Training_session add constraint FK_Reference_10 foreign key (EP_plan_id)
      references Exercise_plan (EP_plan_id);

