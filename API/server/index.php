<?php
require_once __DIR__ . '/vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Silex\Application;

$app = new Silex\Application();


$app->DELETE('/api/v1//delete/{attempt_id}', function(Application $app, Request $request, $attempt_id) {
            
            
            return new Response('How about implementing deleteAttemptById as a DELETE method ?');
            });


$app->GET('/api/v1//attempt/{attempt_id}', function(Application $app, Request $request, $attempt_id) {
            
            
            return new Response('How about implementing getAttemptById as a GET method ?');
            });


$app->POST('/api/v1//measurement', function(Application $app, Request $request) {
            
            
            return new Response('How about implementing addMeasurement as a POST method ?');
            });


$app->DELETE('/api/v1//measurement/{measurement_id}', function(Application $app, Request $request, $measurement_id) {
            
            
            return new Response('How about implementing deleteMeasurementById as a DELETE method ?');
            });


$app->GET('/api/v1//measurement/user/{user_id}', function(Application $app, Request $request, $user_id) {
            
            
            return new Response('How about implementing deleteMeasurementByUser as a GET method ?');
            });


$app->GET('/api/v1//measurement/{measurement_id}', function(Application $app, Request $request, $measurement_id) {
            
            
            return new Response('How about implementing getMeasurementById as a GET method ?');
            });


$app->POST('/api/v1//plan', function(Application $app, Request $request) {
            
            
            return new Response('How about implementing addPlan as a POST method ?');
            });


$app->PUT('/api/v1//plan', function(Application $app, Request $request) {
            
            
            return new Response('How about implementing changePlan as a PUT method ?');
            });


$app->DELETE('/api/v1//plan/{plan_id}', function(Application $app, Request $request, $plan_id) {
            
            
            return new Response('How about implementing deletePlanById as a DELETE method ?');
            });


$app->GET('/api/v1//plan/user/{user_id}', function(Application $app, Request $request, $user_id) {
            
            
            return new Response('How about implementing deletePlanByUser as a GET method ?');
            });


$app->GET('/api/v1//plan/{plan_id}', function(Application $app, Request $request, $plan_id) {
            
            
            return new Response('How about implementing getPlanById as a GET method ?');
            });


$app->POST('/api/v1//training_session/{training_session_id}/add_attempt', function(Application $app, Request $request, $training_session_id) {
            
            
            return new Response('How about implementing addAttempt as a POST method ?');
            });


$app->GET('/api/v1//training_session/user/{user_id}', function(Application $app, Request $request, $user_id) {
            
            
            return new Response('How about implementing deleteSessioanByUser as a GET method ?');
            });


$app->DELETE('/api/v1//training_session/{training_session_id}', function(Application $app, Request $request, $training_session_id) {
            
            
            return new Response('How about implementing deleteSessionById as a DELETE method ?');
            });


$app->PUT('/api/v1//training_session/{training_session_id}/end', function(Application $app, Request $request, $training_session_id) {
            
            
            return new Response('How about implementing endSession as a PUT method ?');
            });


$app->GET('/api/v1//training_session/{training_session_id}', function(Application $app, Request $request, $training_session_id) {
            
            
            return new Response('How about implementing geteSessionById as a GET method ?');
            });


$app->PUT('/api/v1//training_session/start', function(Application $app, Request $request) {
            
            
            return new Response('How about implementing startSession as a PUT method ?');
            });


$app->DELETE('/api/v1//user/{username}', function(Application $app, Request $request, $username) {
            
            
            return new Response('How about implementing deleteUser as a DELETE method ?');
            });


$app->GET('/api/v1//user/{username}', function(Application $app, Request $request, $username) {
            
            
            return new Response('How about implementing getUserByName as a GET method ?');
            });


$app->GET('/api/v1//user/login', function(Application $app, Request $request) {
            $username = $request->get('username');    $password = $request->get('password');    
            
            return new Response('How about implementing loginUser as a GET method ?');
            });


$app->PUT('/api/v1//user/{username}', function(Application $app, Request $request, $username) {
            
            
            return new Response('How about implementing updateUser as a PUT method ?');
            });


$app->run();
